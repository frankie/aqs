package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// httperr is a (minimal) RFC 7807 problem object
type httperr struct {
	Detail string `json:"detail"`
}

// Returns the value of the API_URL environment variable -if set- or a reasonable
// default otherwise. The supplied suffix value is appended to the resulting URL.
func apiTestURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/pageviews"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", res, suffix)
}

func TestPerArticle(t *testing.T) {

	// Requesting a known set of results (daily granularity)
	t.Run("daily", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("per-article/en.wikipedia/all-access/all-agents/Banana/daily/20190101/20190102"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerArticleResponse
		// so unmarshal it to create an object of type PerArticleResponse.  Error-out if that fails.
		obj := PerArticleResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 2, "Invalid number of results")
	})

	// Requesting a known set of results (daily granularity), with an upper-cased project name, and .org suffix.
	// This test should succeed (the service should normalize the project name accordingly), so it will look
	// pretty much like the test above.
	t.Run("project normalization", func(t *testing.T) {
		// res, err := http.Get(apiTestURL("per-article/EN.WIKIPEDIA.ORG/all-access/all-agents/Banana/daily/20190101/20190102"))
		// require.NoError(t, err, "HTTP request error")
	})

	// Request with an invalid timestamp (should fail with return status code 400)
	t.Run("invalid timestamp", func(t *testing.T) {
		// res, err := http.Get(apiTestURL("per-article/en.wikipedia/all-access/all-agents/Banana/daily/20190101/201901a2"))
		// require.NoError(t, err, "HTTP request error")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct (see top of file), similar
		// to the way we unmarshalled to PerArticleResponse in the test above.  Validating that the
		// resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
	})
}
